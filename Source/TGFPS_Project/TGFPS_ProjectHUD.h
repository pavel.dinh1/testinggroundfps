// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TGFPS_ProjectHUD.generated.h"

UCLASS()
class ATGFPS_ProjectHUD : public AHUD
{
	GENERATED_BODY()

public:
	ATGFPS_ProjectHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

