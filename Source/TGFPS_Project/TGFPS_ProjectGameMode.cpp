// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TGFPS_ProjectGameMode.h"
#include "TGFPS_ProjectHUD.h"
#include "TGFPS_ProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATGFPS_ProjectGameMode::ATGFPS_ProjectGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATGFPS_ProjectHUD::StaticClass();
}
