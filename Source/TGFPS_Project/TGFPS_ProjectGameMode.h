// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TGFPS_ProjectGameMode.generated.h"

UCLASS(minimalapi)
class ATGFPS_ProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATGFPS_ProjectGameMode();
};



