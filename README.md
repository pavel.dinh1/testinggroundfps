# TestingGroundFPS

### Udemy course from Unreal Engine C++ Developer (Archived) Course
### Updated to Unreal version 4.23.1

## Concept
* You're a contestant in an entertainment game
* Survive as many testing grounds as possible
* Can you reach the end? Is there even an end ?
* You can use force or stealth to pass the frounds

## Rules
* You can pass a testing ground undetected
* If detected you must clear the area
* You're scored on how many areas you pass

## Requirements
* Testing ground scenery
* Props and textures
* A rigged humanoid character we can modify
* Gun and projectile meshes
* Various SFX, samples from Gamemaster Audio

## Potential Technical Challange
* Procedural generation of testing grounds
* Controlling skeletal animations
* AI Behaviour and awareness
* Tracking large assets in Git
